# README #

### What is this repository for? ###

Creating a confetti cannon on your page.

### How do I get set up? ###

Include the script and run 

	Confettio.start(your-settings, "your-ID");
	
You can also run it without and ID, in which case a random ID will be made for you:

	Confettio.start(your-settings);

You can run the default settings by doing: 

	Confettio.start(Confettio.settings, "myConfettio");
	
Or you can make your own like this:

	var myConfettio = {
		parent: document.getElementById("confettiContainer"),
		confettiSettings: {
			amount: 100,
			class: "confetti",
			randomColor: false,
			colorArray: ["#6eff8f", "#2a8cfd"],		
			rotate: true,
			size: {
				w: 20,
				h: 20,
			},
		},
		position: {
			x: -40,
			y: -40,
		},
		power: {
			min: 10,
			max: 80,
		},
		direction: {
			angle: 340,
			spread: 100,
		},
		scene: {
			interval: 10,
			durationMs: 2500,
			fadeoutMs: 500,
			gravity: 5.8,
		}
	};
	
And then launch it by doing:

	Confettio.start(myConfettio, "myConfettio3");

Or you can create a new set of settings based on the defaults like this:

	var newConfettioSettings = new Confettio.settings();
	newConfettioSettings.confettiSettings.amount = 10;
	
And then launch it by doing:

	Confettio.start(newConfettioSettings, "myConfettio1");

