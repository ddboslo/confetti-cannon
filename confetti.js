var Confettio = (function(){
	var defaultOptions = function settings(){
		this.parent = document.body;
		this.confettiSettings = {
			amount: 100,
			class: "confetti",
			image: false,
			imageArray: [],
			randomColor: true, //COMPLETE RANDOM COLOR OR FROM COLOR ARRAY
			colorArray: ["#000000", "#ff00ff"],	
			rotate: true,
			size: {
				w: 20,
				h: 20,
			},
		};
		this.position = {
			x: 0,
			y: 0,
		};
		this.power = {
			min: 20,
			max: 40,
		};
		this.direction = {
			angle: 340,
			spread: 60,
		};
		this.scene = {
			interval: 10,
			durationMs: 2000,
			fadeoutMs: 1000,
			gravity: 5.8,
		};
	};	
	
	var startConfettio = function(cannon, id){
		if(id === undefined){
			id = Math.floor((Math.random() * 9999999999) + 1);
		}
		if(document.getElementById(id)){
			console.log("Confettio start ignored, this ID is currently in use");
		}
		else{
			var confetti = [];	
			createConfettio(cannon, id);
			createAllConfetti(cannon, confetti, id);
			var time = 0;
			var timer = setInterval(function(){ 
				updateConfetti(cannon, time, confetti, id); 
				time++; 
			}, cannon.scene.interval);
			setTimeout(function(){
				endConfettio(cannon, timer, id);
			},
			cannon.scene.durationMs);
		}
	};

	var createConfettio = function(cannon, id){
		var parent = cannon.parent;
		var confettiContainer = document.createElement("div");	
		confettiContainer.id = id;
		confettiContainer.style.position = "absolute";
		confettiContainer.style.left = cannon.position.x+"px";
		confettiContainer.style.top = cannon.position.y+"px";
		confettiContainer.style.transition = cannon.scene.fadeoutMs/1000+"s";
		parent.appendChild(confettiContainer);
	};

	var createAllConfetti = function(cannon, confetti, id){	
		for(var i = 0; i < cannon.confettiSettings.amount; i++){
			createConfetti(cannon, confetti, id);
		}
	};

	var createConfetti = function(cannon, confetti,id){
		var className = cannon.confettiSettings.class;
		var parentId = id;
		var confettiDom = document.createElement("div");
		if(cannon.confettiSettings.image == false){
			if(cannon.confettiSettings.randomColor == true){
				confettiDom.style.backgroundColor = getRandomColor();
			}
			else{
				var randomColor = Math.floor(Math.random() * cannon.confettiSettings.colorArray.length);
				confettiDom.style.backgroundColor = cannon.confettiSettings.colorArray[randomColor];
			}
		}
		else{
			var randomImage = Math.floor(Math.random() * cannon.confettiSettings.imageArray.length);
			confettiDom.style.backgroundImage = "url('"+cannon.confettiSettings.imageArray[randomImage]+"')";
		}
		confettiDom.style.width = cannon.confettiSettings.size.w+"px";
		confettiDom.style.height = cannon.confettiSettings.size.h+"px";
		confettiDom.style.transformOrigin = "center center";
		confettiDom.className = className+" "+className+parentId;
		confettiDom.style.position = "absolute";
		var singleConfetti = {
			position: {
				x: 0,
				y: 0
			},
			angle: Math.floor(Math.random() * cannon.direction.spread)+(cannon.direction.angle - cannon.direction.spread/2),
			power: Math.floor(Math.random() * (cannon.power.max - cannon.power.min))+cannon.power.min,
			rotation: Math.floor(Math.random() * 360),
		};
		confetti.push(singleConfetti);
		document.getElementById(parentId).appendChild(confettiDom);	
	};

	var updateConfetti = function(cannon, time, confetti, id){
		for(var i = 0; i < confetti.length; i++){
			var confettiData = confetti[i];
			var confettiDOM = document.getElementsByClassName(cannon.confettiSettings.class+id)[i];
			var newPos = getNewPos(confettiData.angle, confettiData.power, time/10, cannon.scene.gravity);
			confettiData.position.x = newPos[0];
			confettiData.position.y = newPos[1];
			if(cannon.confettiSettings.rotate == true){
				confettiData.rotation += 1;
			}
			setConfettiDOMPosition(cannon.confettiSettings.rotate, confettiDOM, confettiData);
		}
	};

	var endConfettio = function(cannon, timer, id){
		var element = document.getElementById(id);
		element.style.opacity = 0;		
		setTimeout(function(){
			clearInterval(timer);
			element.parentNode.removeChild(element);		
		}, cannon.scene.fadeoutMs);
	};

	var setConfettiDOMPosition = function(rotate, objectDOM, objectData){
		if(rotate == true){		
			objectDOM.style.transform = "rotate3d("+1+", "+1+", "+1+", "+objectData.rotation+"deg)";
			objectDOM.style.webkitTransfrom = "rotate3d("+1+", "+1+", "+1+", "+objectData.rotation+"deg)";
			}
			objectDOM.style.left = objectData.position.x+"px";
			objectDOM.style.bottom = objectData.position.y+"px";			
			//objectDOM.style.transform = "translate("+objectData.position.x+"px,"+-1*objectData.position.y+"px"+")";
	};

	var getNewPos = function(ag, v, tt, g){	
		var newX = vxf(v,ag)*tt;	
		var newY = yt(v,ag,tt,g);
		var newPos = [newX, newY];
		return newPos
	};

	var vxf = function(v,a){
		return v*Math.cos(a*Math.PI/180);
	};

	var yt = function(v,a,t,g){
		return v*Math.sin(a*Math.PI/180)*t-.5*g*t*t;	
	};

	var getRandomColor = function(){
		var letters = '0123456789ABCDEF';
		var color = '#';
		for (var i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		return color;
	}
	return {start:startConfettio, settings:defaultOptions};
})();